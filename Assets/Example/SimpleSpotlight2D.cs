using System.Collections.Generic;
using System.Linq;
using Rendering.Lighting.Code;

namespace Example
{
    public class SimpleSpotlight2D : Spotlight2D
    {
        public override List<LitRenderer> GetAllLitRenderers()
        {
            return FindObjectsOfType<LitRenderer>().ToList();
        }
    }
}