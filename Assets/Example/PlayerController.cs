﻿using Physics;
using Rendering.Lighting.Code;
using UnityEngine;

namespace GamePlay
{
    public class PlayerController : PhysicsObject
    {
        [SerializeField] private float _maxSpeed = 7;
        [SerializeField] private float _jumpTakeOffSpeed = 7;
        [SerializeField] private float _lightFocusSpeed = 2.5f;
        [SerializeField] private SpriteRenderer _spriteRenderer;
        [SerializeField] private Animator _animator;
        [SerializeField] private Spotlight2D _spotlight;
        [SerializeField] private Camera _camera;

        private int _animatorWalkHash;

        private void Awake()
        {
            _animatorWalkHash = Animator.StringToHash("Walk");
        }

        protected override void CalculateVelocity()
        {
            Vector2 move = Vector2.zero;

            if (Input.GetKey(KeyCode.D))
            {
                move.x = 1;
            }
            else if (Input.GetKey(KeyCode.A))
            {
                move.x = -1;
            }

            if (Input.GetKeyDown(KeyCode.Space) && IsGrounded)
            {
                Velocity.y = _jumpTakeOffSpeed;
            }
            else if (Input.GetKeyUp(KeyCode.Space))
            {
                if (Velocity.y > 0)
                {
                    Velocity.y *= 0.5f;
                }
            }

            if (Input.GetMouseButton(0))
            {
                _spotlight.DecreaseAngle(_lightFocusSpeed);
            }

            if (Input.GetMouseButton(1))
            {
                _spotlight.IncreaseAngle(_lightFocusSpeed);
            }

            var mousePosition = _camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y,
                _spotlight.transform.position.z));
            var mouseDirection = mousePosition - _spotlight.transform.position;
            var mouseAngle = Mathf.Atan2(mouseDirection.y, mouseDirection.x) * Mathf.Rad2Deg;
            _spotlight.transform.rotation = Quaternion.Euler(0, 0, mouseAngle);

            if (mouseDirection.x < -0.01f)
            {
                _spriteRenderer.transform.rotation = Quaternion.Euler(0, 180, 0);
            }
            else if (mouseDirection.x > 0.01f)
            {
                _spriteRenderer.transform.rotation = Quaternion.Euler(0, 0, 0);
            }

            _animator.SetBool(_animatorWalkHash, Mathf.Abs(move.x) > 0.01f);
            TargetVelocity = move * _maxSpeed;
        }
    }
}