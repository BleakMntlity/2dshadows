using UnityEngine;

namespace Debugging
{
    public class TimeScaleDebugger : MonoBehaviour
    {
        [Range(0, 10f)] public float m_timeScale = 1;

        private void OnValidate()
        {
            Time.timeScale = m_timeScale;
        }
    }
}