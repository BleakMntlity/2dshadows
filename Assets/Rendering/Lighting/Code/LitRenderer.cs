﻿using UnityEngine;

namespace Rendering.Lighting.Code
{
    public class LitRenderer : MonoBehaviour
    {
        public SpriteRenderer Renderer;
        public Color ShadowColor = Color.black;
        public Mesh ShadowMesh;
        
        private static int LitForegroundLayer = -1;

        public bool ShadowCaster
        {
            get { return Renderer.sortingLayerID == LitForegroundLayer; }
        }

        private void Awake()
        {
            if (LitForegroundLayer == -1)
            {
                LitForegroundLayer = SortingLayer.NameToID("Lit Foreground");
            }
        }

        #region Editor        

#if UNITY_EDITOR
        public bool GenerateShadowMesh;
        public bool DrawGizmos;

        protected virtual void GenerateShadowDataEditor()
        {
            var polygonCollider = gameObject.AddComponent<PolygonCollider2D>();
            ShadowMesh = ShadowVolumeUtil.CreateMeshWithoutProjection(polygonCollider);
            UnityEditor.EditorApplication.delayCall += () => DestroyImmediate(polygonCollider);
        }

        private void OnValidate()
        {
            if (GenerateShadowMesh)
            {
                GenerateShadowMesh = false;
                GenerateShadowDataEditor();
            }
        }

        private void OnDrawGizmosSelected()
        {
            if (ShadowMesh == null || !DrawGizmos)
                return;

            var spotlight2DPos = FindObjectOfType<Spotlight2D>().transform.position;
            for (int i = 0; i < ShadowMesh.triangles.Length; i += 3)
            {
                var first = transform.TransformPoint(ShadowMesh.vertices[ShadowMesh.triangles[i]]);
                var second = transform.TransformPoint(ShadowMesh.vertices[ShadowMesh.triangles[i + 1]]);
                var third = transform.TransformPoint(ShadowMesh.vertices[ShadowMesh.triangles[i + 2]]);

                if (ShadowMesh.uv[ShadowMesh.triangles[i]][0] > 0)
                    first += (first - spotlight2DPos) * 10;

                if (ShadowMesh.uv[ShadowMesh.triangles[i + 1]][0] > 0)
                    second += (second - spotlight2DPos) * 10;

                if (ShadowMesh.uv[ShadowMesh.triangles[i + 2]][0] > 0)
                    third += (third - spotlight2DPos) * 10;

                Gizmos.DrawLine(first, second);
                Gizmos.DrawLine(first, third);
                Gizmos.DrawLine(second, third);
            }
        }
#endif

        #endregion
    }
}