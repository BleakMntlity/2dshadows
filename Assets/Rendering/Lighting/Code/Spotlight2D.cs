﻿using System.Collections.Generic;
using GamePlay;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Serialization;

namespace Rendering.Lighting.Code
{
    public abstract class Spotlight2D : MonoBehaviour
    {
        [SerializeField] [FormerlySerializedAs("Range")]
        private float _range;

        [SerializeField] [FormerlySerializedAs("Angle")]
        private float _angle;

        public Color Color;
        public Camera SceneCamera;
        public Material ShadowMaterial;
        public Material LightMaterial;
        public Material StencilMaterial;
        [Range(0, 1)] public float AmbientLight;
        public Color AmbientColor;
        public RenderTexture ShadowMap;

        [SerializeField] private float _strength;
        [SerializeField] [Range(0.1f, 179.9f)] private float _maxAngle;
        [SerializeField] [Range(0.1f, 179.9f)] private float _minAngle;

        private readonly Color _clearColor = Color.black;
        private readonly Vector3[] _lightMeshVertices = new Vector3[3];
        
        private CommandBuffer _shadowPass;
        private Mesh _lightMesh;

        public abstract List<LitRenderer> GetAllLitRenderers();

        public void DecreaseAngle(float decreaseBy)
        {
            _angle = Mathf.Max(_angle - decreaseBy, _minAngle);
        }

        public void IncreaseAngle(float increaseBy)
        {
            _angle = Mathf.Min(_angle + increaseBy, _maxAngle);
        }

        public bool InsideLightCone(Bounds bounds)
        {
            Vector2 upper, lower;
            GetLightDirection(out upper, out lower);

            Vector2 upperBound, lowerBound;
            GetDirectionToUpperAndLowerBounds(bounds, out upperBound, out lowerBound);

            var distanceUpper = upperBound.magnitude;
            var distanceLower = lowerBound.magnitude;
            if (distanceUpper > _range && distanceLower > _range)
                return false;

            var hit1 = upperBound.x < 0
                ? upperBound.y <= (lower * distanceUpper).y && lowerBound.y >= (upper * distanceLower).y
                : upperBound.y <= (upper * distanceUpper).y && lowerBound.y >= (distanceLower * lower).y;
            var hit2 = lowerBound.x < 0
                ? lowerBound.y <= (distanceLower * upper).y && upperBound.y >= (lower * distanceUpper).y
                : lowerBound.y <= (lower * distanceLower).y && upperBound.y >= (upper * distanceUpper).y;

            return hit1 || hit2;
        }

        public virtual float GetLightIntensity(float distance)
        {
            return _minAngle / _angle * _strength * Mathf.Max(1 - distance / _range, 0);
        }

        private void Start()
        {
            _lightMesh = new Mesh
            {
                vertices = new[] {Vector3.zero, Vector3.zero, Vector3.zero},
                normals = new Vector3[3],
                triangles = new[] {2, 1, 0}
            };

            _shadowPass = new CommandBuffer {name = "ShadowPass"};
            SceneCamera.AddCommandBuffer(CameraEvent.BeforeForwardOpaque, _shadowPass);

            Shader.SetGlobalColor("_AmbientLight", AmbientLight * AmbientColor);
            Shader.SetGlobalColor("_SpotlightColor", Color);
            Shader.SetGlobalTexture("_ShadowMap", ShadowMap);
        }

        private void LateUpdate()
        {
            PrepareLightPass();
            CalculateLightShape();
            _shadowPass.SetGlobalVector("_LightPos", transform.position);
            _shadowPass.SetGlobalFloat("_LightRange", _range);

            _shadowPass.DrawMesh(_lightMesh, Matrix4x4.identity, LightMaterial);

            var litRenderers = GetAllLitRenderers();
            foreach (var litRenderer in litRenderers)
            {
                if (!litRenderer.ShadowCaster || !litRenderer.isActiveAndEnabled)
//                    || !litRenderer.Renderer.isVisible || !InsideLightCone(litRenderer.Collider.bounds))
                    continue;

                var shadow = litRenderer.ShadowMesh;
                if (shadow == null)
                    continue;

                _shadowPass.DrawRenderer(litRenderer.Renderer, StencilMaterial);
                _shadowPass.SetGlobalColor("_ShadowColor", litRenderer.ShadowColor);
                _shadowPass.DrawMesh(shadow, litRenderer.transform.localToWorldMatrix, ShadowMaterial);
                _shadowPass.ClearRenderTarget(true, false, _clearColor);
            }
        }

        private void GetDirectionToUpperAndLowerBounds(Bounds bounds, out Vector2 upper, out Vector2 lower,
            float deviation = 0)
        {
            upper = new Vector2(bounds.center.x, bounds.max.y);
            lower = new Vector2(bounds.center.x, bounds.min.y);

            upper.y -= deviation;
            lower.y += deviation;

            upper -= new Vector2(transform.position.x, transform.position.y);
            lower -= new Vector2(transform.position.x, transform.position.y);
        }

        private void CalculateLightShape()
        {
            Vector2 upper;
            Vector2 lower;
            GetLightDirection(out upper, out lower);
            Vector2 twoDpos = transform.position;
            var projectedUpper = twoDpos + (upper * _range);
            var projectedLower = twoDpos + (lower * _range);

            _lightMeshVertices[0] = twoDpos;
            _lightMeshVertices[1] = projectedLower;
            _lightMeshVertices[2] = projectedUpper;
            _lightMesh.vertices = _lightMeshVertices;
        }

        private void GetLightDirection(out Vector2 upper, out Vector2 lower)
        {
            var lightRotation = transform.rotation.eulerAngles.z;
            var halfAngle = _angle / 2;
            upper.x = Mathf.Cos((lightRotation + halfAngle) * Mathf.Deg2Rad);
            upper.y = Mathf.Sin((lightRotation + halfAngle) * Mathf.Deg2Rad);
            lower.x = Mathf.Cos((lightRotation - halfAngle) * Mathf.Deg2Rad);
            lower.y = Mathf.Sin((lightRotation - halfAngle) * Mathf.Deg2Rad);
        }

        private void PrepareLightPass()
        {
            _shadowPass.Clear();
            _shadowPass.SetRenderTarget(ShadowMap);
            _shadowPass.ClearRenderTarget(true, true, _clearColor);
        }

        #region Editor

#if UNITY_EDITOR
        public bool DrawLightCone;

        private void OnDrawGizmos()
        {
            if (DrawLightCone && _lightMesh != null)
            {
                Gizmos.color = Color;
                Gizmos.DrawWireMesh(_lightMesh);
            }
        }

        private void OnValidate()
        {
            Shader.SetGlobalColor("_AmbientLight", AmbientLight * AmbientColor);
            Shader.SetGlobalColor("_SpotlightColor", Color);
        }
#endif

        #endregion
    }
}