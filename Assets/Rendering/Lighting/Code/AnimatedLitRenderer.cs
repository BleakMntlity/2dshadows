using UnityEngine;


namespace Rendering.Lighting.Code
{
    public class AnimatedLitRenderer : LitRenderer
    {
        [SerializeField] private SpriteAndMeshSheet _spriteMeshSheet;

        public void OnSpriteChanged()
        {
            ShadowMesh = _spriteMeshSheet.FindForSprite(Renderer.sprite) ?? ShadowMesh;
        }


        #region Editor

#if UNITY_EDITOR
        public Texture2D EditorSpriteSheet;

        protected override void GenerateShadowDataEditor()
        {
            _spriteMeshSheet = gameObject.GetComponent<SpriteAndMeshSheet>();
            if (_spriteMeshSheet != null)
                _spriteMeshSheet.Clear();
            else
                _spriteMeshSheet = gameObject.AddComponent<SpriteAndMeshSheet>();

            var spriteSheetPath = UnityEditor.AssetDatabase.GetAssetPath(EditorSpriteSheet);
            var sprites = UnityEditor.AssetDatabase.LoadAllAssetsAtPath(spriteSheetPath);
            for (int i = 0; i < sprites.Length; ++i)
            {
                var sprite = sprites[i] as Sprite;
                if (sprite == null)
                    continue;

                Renderer.sprite = sprite;
                var polygonCollider = gameObject.AddComponent<PolygonCollider2D>();
                var shadowMesh = ShadowVolumeUtil.CreateMeshWithoutProjection(polygonCollider);
                _spriteMeshSheet.Add(sprite, shadowMesh);
                UnityEditor.EditorApplication.delayCall += () => DestroyImmediate(polygonCollider);
            }
        }
#endif

        #endregion
    }
}