using System;
using System.Collections.Generic;
using UnityEngine;

namespace Rendering.Lighting.Code
{
    [Serializable]
    public class SpriteAndCollider : SpriteAndSomething<PolygonCollider2D>
    {
        [SerializeField] private PolygonCollider2D _collider;

        public override PolygonCollider2D Something
        {
            get { return _collider; }
            set { _collider = value; }
        }
    }

    public class SpriteAndColliderSheet : SpriteAndSomethingSheet<PolygonCollider2D>
    {
        [SerializeField] private List<SpriteAndCollider> _colliders;

        public override void Add(Sprite sprite, PolygonCollider2D element)
        {
            var pair = new SpriteAndCollider
            {
                Sprite = sprite,
                Something = element
            };
            _colliders.Add(pair);
        }

        public override int Count()
        {
            return _colliders.Count;
        }

        public override SpriteAndSomething<PolygonCollider2D> GetElement(int i)
        {
            return _colliders[i];
        }
    }
}