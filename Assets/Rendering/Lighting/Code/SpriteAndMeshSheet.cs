using System;
using System.Collections.Generic;
using UnityEngine;

namespace Rendering.Lighting.Code
{
    [Serializable]
    public class SpriteAndMesh : SpriteAndSomething<Mesh>
    {
        [SerializeField] private Mesh _mesh;

        public override Mesh Something
        {
            get { return _mesh; }
            set { _mesh = value; }
        }
    }

    public class SpriteAndMeshSheet : SpriteAndSomethingSheet<Mesh>
    {
        [SerializeField] private List<SpriteAndMesh> _meshes = new List<SpriteAndMesh>();

        public override void Add(Sprite sprite, Mesh element)
        {
            var pair = new SpriteAndMesh()
            {
                Sprite = sprite,
                Something = element
            };
            _meshes.Add(pair);
        }

        public override int Count()
        {
            return _meshes.Count;
        }

        public override SpriteAndSomething<Mesh> GetElement(int i)
        {
            return _meshes[i];
        }

        public void Clear()
        {
            _meshes.Clear();
        }
    }
}